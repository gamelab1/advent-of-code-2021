use std::str::FromStr;
use std::fs;

enum Direction {
    Up(u32),
    Down(u32),
    Forward(u32)
}

impl FromStr for Direction {
    type Err = ();

    fn from_str(input: &str) -> Result<Direction, Self::Err> {
        let mut parts = input.split_whitespace();

        let dir_amount = (parts.next().unwrap(), parts.next().unwrap().parse::<u32>().unwrap());

        match dir_amount {
            ("up", x)  => Ok(Direction::Up(x)),
            ("down", x)  => Ok(Direction::Down(x)),
            ("forward", x)  => Ok(Direction::Forward(x)),
            _      => Err(()),
        }
    }
}

fn main() {
    let mut position = (0,0);

    //Part 1

    let contents = fs::read_to_string("src/input")
        .expect("Something went wrong reading the file");

    let mut directions_iter = contents.as_str().split_terminator("\n")
        .map(|d| {
            
            Direction::from_str(d).unwrap()
        });

    while let Some(d) = directions_iter.next() {
        match d {
            Direction::Up(a) => position.0 -= a,
            Direction::Down(a) => position.0 += a,
            Direction::Forward(a) => position.1 += a,
        }
    }

    //Part 2

    let mut position_2 = (0,0,0);

    position_2 = contents.as_str().split_terminator("\n")
        .map(|d| Direction::from_str(d).unwrap())
        .fold((0,0,0), |mut acc, d| {

            match d {
                Direction::Up(a) => acc.2 -= a,
                Direction::Down(a) => acc.2 += a,
                Direction::Forward(a) => {acc.1 += a; acc.0 += acc.2 * a},
            }
            
            acc
        });



    println!("{:?} - multiplied : {}  -- {:?} - multiplied : {}", position, position.0 * position.1, position_2, position_2.0 * position_2.1);
    

}
