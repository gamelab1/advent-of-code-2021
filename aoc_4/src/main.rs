use std::fs;

#[derive(Debug)]
struct Card {
    rows : Vec<Vec<(u32, bool)>>
}

impl Clone for Card {
    fn clone(&self) -> Card {
        let mut new_card = Card::new();

        for r in self.rows.as_slice() {
            new_card.rows.push(r.iter().map(|t| t.clone()).collect())
        }

        new_card
    }
}

impl Card {
    fn new() -> Card {
        Card {
            rows: vec![]
        }
    }

    fn add_row(&mut self, row : &str) {
        let row_n = row.split_whitespace()
            .map(|s : &str| (s.parse::<u32>().unwrap(), false))
            .collect::<Vec<(u32, bool)>>();

        self.rows.push(row_n);
    }

    fn sign_number(&mut self, n: u32) {
        for r in self.rows.as_mut_slice() {
           for p in r.as_mut_slice() {
                if p.0 == n {
                    *p = (n, true);
                }
           }
        }
    }

    fn check(&self) -> bool {
        let mut rows_consumer = self.rows.iter();

        while let Some(r) = rows_consumer.next() {
            if r.iter().all(|p| p.1) {
                return true
            }
        }

        for i in 0..self.rows[0].len() {
            let mut res = true;
            for j in 0..self.rows.len() {
                res &= self.rows[j][i].1;
            }

            if res {
                return true;
            }
        }

        false
    }

    fn get_winning_line(&self) -> Option<&Vec<(u32,bool)>> {
        for r in self.rows.iter() {
            if r.iter().all(|cell| cell.1) {
                return Some(&r);
            }
        }

        None
    }
}

fn main() {

    let content = fs::read_to_string("src/input").expect("Error while reading the file");

    //Part 1

    // let mut lines = content.split_terminator("\n");

    // let numbers = lines.next().unwrap();

    // let mut cards : Vec<Card> = vec![];

    // let mut cards_pool : Vec<&str> = vec![];

    // while let Some(l) = lines.next() {
    //     match l.trim() {
    //         "" => (),
    //         a => cards_pool.push(a)
    //     }

    //     if cards_pool.len() == 5 {
    //         let mut card = Card::new();
    //         while let Some(ll) = cards_pool.pop() {
    //             card.add_row(ll);
    //         }

    //         cards.push(card);
    //     }
    // }

    // let mut extractions = numbers.split_terminator(",");
    // let mut winning_number : Option<u32> = None;
    // let mut winning_card : Option<Card> = None;

    // 'extraction: while let Some(n) = extractions.next() {
    //     for c in cards.as_mut_slice() {
    //         c.sign_number(n.parse::<u32>().unwrap());

    //         if c.check() {
    //             winning_number = Some(n.parse::<u32>().unwrap());
    //             winning_card.replace(c.clone());
    //             break 'extraction;
    //         }
    //     }
    // }

    // match winning_card {
    //     Some(c) => {
    //         println!("{}\n\nWinning card: {:?}\n\nWinning number: {}\n\nWinning line: {:?}", 
    //             numbers, 
    //             c, 
    //             winning_number.unwrap(),
    //             c.get_winning_line()
    //         );

    //         let total_unmarked = c.rows.iter()
    //             .map(|row| row.iter().filter(|(_,v)| !v).fold(0, |acc, x| acc + x.0))
    //             .sum::<u32>();
            
    //         println!("{} {}", total_unmarked, winning_number.unwrap() * total_unmarked)
    //     }
    //     None => ()
    // }
 
    
    //Part 2

    let mut lines = content.split_terminator("\n");

    let numbers = lines.next().unwrap();

    let mut cards : Vec<Option<Card>> = vec![];

    let mut cards_pool : Vec<&str> = vec![];

    while let Some(l) = lines.next() {
        match l.trim() {
            "" => (),
            a => cards_pool.push(a)
        }

        if cards_pool.len() == 5 {
            let mut card = Card::new();
            while let Some(ll) = cards_pool.pop() {
                card.add_row(ll);
            }

            cards.push(Some(card));
        }
    }

    let mut extractions = numbers.split_terminator(",");

    let mut last_winning_card : Option<Card> = None;
    let mut winning_number : Option<u32> = None;

    while let Some(n) = extractions.next() {

        let cards_filtered = cards.iter_mut();
        let mut winning_cards : usize = 0;

        for c in cards_filtered {
            match c {
                Some(cc) => {
                    cc.sign_number(n.parse::<u32>().unwrap());

                    if cc.check() {
                        winning_number = Some(n.parse::<u32>().unwrap());
                        winning_cards += 1;
                        last_winning_card = c.take();
                    }
                },
                None => ()
            }
        }
    }

    match last_winning_card {
        Some(c) => {
            println!("Extracted numbers # {}\n\nLast winning card: {:?}\n\nWinning number: {}\n\nWinning line: {:?}", 
                numbers.split_terminator(",").count(), 
                c, 
                winning_number.unwrap(),
                c.get_winning_line()
            );

            let total_unmarked = c.rows.iter()
                .map(|row| row.iter().filter(|(_,v)| !v).fold(0, |acc, x| acc + x.0))
                .sum::<u32>();
            
            println!("{} {}", total_unmarked, winning_number.unwrap() * total_unmarked)
        }
        None => ()
    }
}
