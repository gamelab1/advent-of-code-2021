use std::collections::HashSet;
use std::collections::HashMap;
use std::fs;
use std::thread;
use itertools::Itertools;

fn main() {

    let content = fs::read_to_string("/Users/mae/Documents/Personal/Programming/rust/aoc_2021/aoc_8/src/input").expect("Expected a correct file path");

    let lines : Vec<&str> = content.split_terminator("\n")
        .map(|l| l.split_once("|"))
        .filter_map(|mut t| t.take())
        .map(|t| t.1.trim())
        .collect();

    let easy_count = lines.iter().fold(0, |acc, x| acc + x.split_whitespace().fold(0,|local_acc, y| {
        local_acc + match y.chars().count() {
            4 | 2 | 3 | 7 => 1,
            _ => 0
        }
    }));

    println!("Easy count -> {}", easy_count);

    // Part 2

    let handlers : Vec<thread::JoinHandle<_>> = content.split_terminator("\n").map(|l| l.split_once("|")).filter_map(|mut t| t.take()).map(|t| {
        let signals = String::from(t.0.trim());
        let digits = String::from(t.1.trim());

        thread::spawn(move || {

            let normalized_digits : Vec<String> = digits.split_whitespace()
                .map(|d| d.chars().sorted().collect::<String>()).collect();

            let dict = get_dictionary(signals.split_whitespace().collect());

            normalized_digits.iter().map(|nd| dict.get(nd).unwrap().as_str()).collect::<Vec<&str>>().join("").parse::<u32>()
        })
    }).collect();

    let mut sum = 0;

    for h in handlers  {
        sum += match h.join().unwrap() {
            Ok(a) => a,
            Err(_) => 0
        }
    }

    println!("Sum {}", sum);
}

fn get_dictionary(signals: Vec<&str>) -> HashMap<String, String> {

    let mut dict_ret : HashMap<u8, HashSet<char>> = HashMap::new();
    let mut dict_resolved : HashMap<String, String> = HashMap::new();

    //Extract 1, 7, 4, 8

    for s in signals.iter() {
        match s.len() {
            2 => {dict_ret.insert(1, s.chars().collect());()},
            3 => {dict_ret.insert(7, s.chars().collect());()},
            4 => {dict_ret.insert(4, s.chars().collect());()},
            7 => {dict_ret.insert(8, s.chars().collect());()},
            _ => ()
        }
    }

    let mut a : HashSet<char> = HashSet::new();
    let mut g : HashSet<char> = HashSet::new();

    a.insert(*dict_ret.get(&7).unwrap().difference(dict_ret.get(&1).unwrap()).next().unwrap());
    
    let four_u_a = dict_ret.get(&4).unwrap().union(&a).map(|c| *c).collect::<HashSet<char>>();

    dict_ret.insert(9, signals.iter()
        .filter(|s| s.len() == 6)
        .filter(|s| s.chars().collect::<HashSet<char>>().difference(&four_u_a).count() == 1)
        .next().unwrap()
        .chars().collect());

    g.insert(*dict_ret.get(&9).unwrap().difference(&four_u_a).next().unwrap());

    let mut e : HashSet<char> = HashSet::new();
    
    e.insert(*dict_ret.get(&8).unwrap().difference(dict_ret.get(&9).unwrap()).next().unwrap()); 

    let seven_g = dict_ret.get(&7).unwrap().union(&g).map(|c| *c).collect::<HashSet<char>>();

    dict_ret.insert(3, signals.iter()
    .filter(|s| s.len() == 5)
    .filter(|s| s.chars().collect::<HashSet<char>>().difference(&seven_g).count() == 1)
    .next().unwrap()
    .chars().collect());

    dict_ret.insert(2, signals.iter()
    .filter(|s| s.len() == 5)
    .filter(|s| e.is_subset(&s.chars().collect::<HashSet<char>>()))
    .next().unwrap()
    .chars().collect());

    dict_ret.insert(5, signals.iter()
    .filter(|s| s.len() == 5)
    .filter(|s| !dict_ret.get(&2).unwrap().is_subset(&s.chars().collect::<HashSet<char>>()) 
    && !dict_ret.get(&3).unwrap().is_subset(&s.chars().collect::<HashSet<char>>()))
    .next().unwrap()
    .chars().collect());

    dict_ret.insert(6, dict_ret.get(&5).unwrap().union(&e).map(|c| *c).collect::<HashSet<char>>());

    dict_ret.insert(0, signals.iter()
    .filter(|s| s.len() == 6)
    .filter(|s| !dict_ret.get(&6).unwrap().is_subset(&s.chars().collect::<HashSet<char>>()) 
    && !dict_ret.get(&9).unwrap().is_subset(&s.chars().collect::<HashSet<char>>()))
    .next().unwrap()
    .chars().collect());

    dict_resolved.extend::<Vec<(String,String)>>(dict_ret.drain().map(|(k,v)| (v.iter().sorted().collect::<String>(), k.to_string())).collect());

    dict_resolved
}