use std::collections::HashMap;
use std::fs;

#[derive(Debug,Clone,Copy)]
struct LanternFish(u8);

fn main() {
    let content = fs::read_to_string("/Users/mae/Documents/Personal/Programming/rust/aoc_2021/aoc_6/src/input").expect("Error while reading the file");
    
    let fishes : Vec<LanternFish> = content.split_terminator(",").map(|s| LanternFish(s.parse::<u8>().unwrap())).collect();

    let mut dict_fishes : HashMap<u8, usize> = HashMap::new();

    //Initial load
    let mut fishes_iter = fishes.iter();

    while let Some(f) = fishes_iter.next() {
        let counter = dict_fishes.entry(f.0).or_insert(0);
        *counter += 1;
    }

    for i in 1..=256 {

        let mut next_round_dict : HashMap<u8,usize> = HashMap::new();
        let mut current_lifetracker = dict_fishes.iter();

        while let Some((life_cycle, count)) = current_lifetracker.next() {
            match life_cycle {
                0 => {
                    let counter = next_round_dict.entry(6).or_insert(0);
                    *counter += count;
                    
                    let new_borns = next_round_dict.entry(8).or_insert(0);
                    *new_borns += count;
                },
                a => {
                    let counter = next_round_dict.entry(a - 1).or_insert(0);
                    *counter += count;
                }
            };
        }

        dict_fishes.clear();

        dict_fishes.extend(next_round_dict.iter());

        println!("After {} days {}", i, dict_fishes.iter().fold(0, |acc, x| acc + x.1));
    }
}
