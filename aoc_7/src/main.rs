use std::collections::HashMap;
use std::fs;
use std::cmp;

fn main() {

    let content = fs::read_to_string("/Users/mae/Documents/Personal/Programming/rust/aoc_2021/aoc_7/src/input").expect("There's been an error while opening the file");

    let crabs_positions : Vec<i32> = content.split_terminator(",").map(|s| s.parse::<i32>().unwrap()).collect();

    let mean = crabs_positions.iter().sum::<i32>() as f32 / crabs_positions.len() as f32;

    let variance = crabs_positions.iter().map(|p| f32::powf((*p as f32) - mean, 2f32)).sum::<f32>() / crabs_positions.len() as f32;

    let std_dev = f32::sqrt(variance);

    let begin_index = cmp::max(0, (mean - std_dev) as i32);

    let end_index = (mean + std_dev).ceil() as i32;

    let mut fuel_expenses_map : HashMap<i32, i32> = HashMap::new();

    println!("mean: {} std: {} max: {} min: {}", mean, std_dev, crabs_positions.iter().max().unwrap(), crabs_positions.iter().min().unwrap());

    for i in begin_index..=end_index {
        for crab_pos in crabs_positions.iter() {
            let e = fuel_expenses_map.entry(i).or_insert(0);
            
            *e += (i - crab_pos).abs();
        }
    }

    let min = fuel_expenses_map.iter().min_by_key(|(_,v)| *v);

    println!("min: {:?}", min.unwrap());

    //Part 2

    let mut fuel_expenses_map_more_expensive : HashMap<i32, i32> = HashMap::new();

    for i in begin_index..=end_index {
        for crab_pos in crabs_positions.iter() {
            let e = fuel_expenses_map_more_expensive.entry(i).or_insert(0);
            
            let distance = (i - crab_pos).abs();

            *e += distance * (distance + 1) / 2;
        }
    }

    let min_expensive = fuel_expenses_map_more_expensive.iter().min_by_key(|(_,v)| *v);

    println!("min: {:?}", min_expensive.unwrap());
}
