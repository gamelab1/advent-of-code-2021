use std::collections::HashMap;
use std::fs;

fn main() {

    //Part 1

    let content = fs::read_to_string("src/input").expect("Got an error while reading the file");

    let lines = content.split_terminator("\n");

    let mut dict : HashMap<char, (char,u32)> = HashMap::new();
    dict.insert(')', ('(',3));
    dict.insert('}', ('{',1197));
    dict.insert(']', ('[',57));
    dict.insert('>', ('<',25137));
    let mut closing_dict : HashMap<char, (char,u64)> = HashMap::new();
    closing_dict.insert('(', (')',1));
    closing_dict.insert('[', (']',2));
    closing_dict.insert('{', ('}',3));
    closing_dict.insert('<', ('>',4));


    let score = lines.enumerate().map(|l| {

        let mut chars = l.1.chars();
        let mut stack : Vec<char> = vec![];

        while let Some(c) = chars.next() {
            match c {
                '(' | '[' | '{' | '<' => stack.push(c),
                ')' | ']' | '}' | '>' => {
                    match stack.pop() {
                        Some(test) => {
                            if dict.get(&c).unwrap().0 != test {    
                                println!("{} -> Expected {}, but found {} instead, valid {} points", 
                                    l.0, 
                                    closing_dict.get(&test).unwrap().0, 
                                    c,
                                    dict.get(&c).unwrap().1
                                );
                                return dict.get(&c).unwrap().1;
                            }
                        },
                        None => panic!("Incomplete!")
                    }
                },
                _ => ()
            }
        }

        0
    }).fold(0, |acc, x| acc + x);

    println!("Error scrore: {}", score);

    //Part 2

    let mut completion_score : Vec<u64> = content.split_terminator("\n").enumerate().map(|l| {

        let mut chars = l.1.chars();
        let mut stack : Vec<char> = vec![];

        let mut skip = false;

        while let Some(c) = chars.next() {
            match c {
                '(' | '[' | '{' | '<' => stack.push(c),
                ')' | ']' | '}' | '>' => {
                    match stack.pop() {
                        Some(test) => {
                            if dict.get(&c).unwrap().0 != test {
                                skip = true;
                            }
                        },
                        None => ()
                    }
                },
                _ => ()
            }
        }

        let mut compliment = (String::from(""), 0u64);

        if stack.len() > 0 && !skip {
            while let Some(s) = stack.pop() {
                compliment.0.push(closing_dict.get(&s).unwrap().0);
                compliment.1 *= 5;
                compliment.1 += closing_dict.get(&s).unwrap().1;
            } 

            println!("{} -> Completed by {} score: {}", l.0, compliment.0, compliment.1);
        }

        compliment.1
    }).filter(|non_zeroes| *non_zeroes != 0u64).collect();

    completion_score.sort();

    println!("Completion scrore: {:?}", completion_score[completion_score.len()/2]);
}
