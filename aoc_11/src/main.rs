use std::collections::HashSet;
use std::fs;


fn main() {

    let content = fs::read_to_string("/Users/mae/Documents/Personal/Programming/rust/aoc_2021/aoc_11/src/input").expect("Expecting correct file path");

    let mut height_map : Vec<Vec<i32>> = content.split_terminator("\n").map(|l| l.chars().map(|c| c.to_digit(10).unwrap() as i32).collect())
        .collect();

    //Padding with 9s
    let cols = height_map[0].len();
    let rows = height_map.len();

    
    for l in height_map.iter_mut() {
        l.insert(0, -1);
        l.push(-1);
    }
    let _first_line = height_map.insert(0, vec![-1; cols + 2]);
    let _last_line = height_map.push(vec![-1; cols + 2]);

    let mut flashes_count = 0;
    let mut steps = 1;
    let mut sim_flashes = loop {
        // for l in height_map.iter() {
        //     println!("{:?}", l.iter().map(|t| t.to_string()).collect::<Vec<String>>().join(","))
        // }

        //Charge
        for i in 1..=rows {
            for j in 1..=cols {
                height_map[i][j] += 1;
            }
        }

        //Flash

        let mut flashed : HashSet<(usize, usize)> = HashSet::new();

        for i in 1..=rows {
            for j in 1..=cols {
                flash((i,j), &mut height_map, &mut flashed);
            }
        }

        println!("{} flashed: {}", steps, flashed.len());

        if steps <= 100 {
            flashes_count += flashed.len();
        }

        if flashed.len() == 100 {
            break steps;
        }

        //Reset
        for i in 1..=rows {
            for j in 1..=cols {
                if height_map[i][j] > 9 {
                    height_map[i][j] = 0;
                }
            }
        }

        steps += 1;
    };

    println!("Total flashes {}", flashes_count);

    //Part 2

    println!("Simoultaneous flashes at step {}", sim_flashes);
}

fn flash(p: (usize,usize), map: &mut Vec<Vec<i32>>, flashed: &mut HashSet<(usize,usize)>) {
    if map[p.0][p.1] <= 9 || flashed.contains(&p) {
        return;
    }
    flashed.insert(p);

    //println!("Flashed {:?}", flashed);
    
    let mut neighbors : HashSet<(usize, usize)> = HashSet::new();

    neighbors.extend(vec![
        (p.0, p.1+1), 
        (p.0+1, p.1+1), 
        (p.0+1, p.1), 
        (p.0+1, p.1-1), 
        (p.0, p.1-1), 
        (p.0-1, p.1-1),
        (p.0-1, p.1),
        (p.0-1, p.1+1)
    ]);

    assert_eq!(neighbors.len(), 8);

    for (y,x) in neighbors {
        match map[y][x] {
            -1 => (),
            a if a >= 9 && !flashed.contains(&(y,x)) => {
                map[y][x] += 1;
                flash((y,x), map, flashed)
            },
            _ => map[y][x] += 1
        }
    }
} 
