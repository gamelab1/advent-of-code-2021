use std::fs;

fn main() {
    let contents = fs::read_to_string("src/input")
        .expect("Something went wrong reading the file");

    //Part a
    
    let increased = contents.as_str().split_terminator("\n").map(|s| s.parse::<u16>().unwrap()).collect::<Vec<u16>>()
        .iter().fold((0, None), |mut acc, x| {
            acc.1 = match acc.1 {
                None => Some(x),
                Some(a) => {
                    if x > a {
                        acc.0 += 1;
                    }

                    Some(x)
                }
            };

            acc
        }).0;

    //Part b

    let lines_vec = contents.as_str().split_terminator("\n").map(|s| s.parse::<u16>().unwrap()).collect::<Vec<u16>>();

    let mut triplet_vec = vec![];
    let mut triplet_summed_vec = vec![];

    let mut it = 0;

    loop {
        let mut local_triplet_iter = lines_vec.iter().skip(it).take(3);

        let triplet = match (local_triplet_iter.next(), local_triplet_iter.next(), local_triplet_iter.next()) {
            (Some(a), Some(b), Some(c)) => (a, b, c),
            _ => break
        };

        triplet_vec.push(triplet);
        triplet_summed_vec.push(triplet.0 + triplet.1 + triplet.2);

        it += 1;
    }

    let increased_triplets = triplet_summed_vec.iter().fold((0, None), |mut acc, x| {
        acc.1 = match acc.1 {
            None => Some(x),
            Some(a) => {
                if x > a {
                    acc.0 += 1;
                }

                Some(x)
            }
        };

        acc
    }).0;

    println!("{:?} {:?} {:?}", increased, increased_triplets, triplet_vec);
}
