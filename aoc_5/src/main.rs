// use std::fmt;
use std::fs;
use std::cmp;

struct Map {
    data: Vec<u32>,
    rows: u32
}

impl Map {
    fn new() -> Map {
        Map {
            rows: 0,
            data: vec![]
        }
    }

    fn add_line(&mut self, line_definition : &str) {
        let parts = line_definition.split_once("->");

        match parts {
            Some((a,b)) => {
                let (x1_s, y1_s) = a.split_once(",").unwrap();
                let (x2_s, y2_s) = b.split_once(",").unwrap();
                
                let (x1,y1) = (x1_s.trim().parse::<i32>().unwrap(),y1_s.trim().parse::<i32>().unwrap());
                let (x2,y2) = (x2_s.trim().parse::<i32>().unwrap(),y2_s.trim().parse::<i32>().unwrap());

                let (ord_x1, ord_x2) = (cmp::min(x1, x2), cmp::max(x1, x2));
                let (ord_y1, ord_y2) = (cmp::min(y1, y2), cmp::max(y1, y2));

                //Extend the map till (ord_x2, ord_y2) if necessary
                let mut cols = if self.rows > 0 {self.data.len() / self.rows as usize} else {0};

                if ord_x2 + 1 > cols as i32 {
                    for i in 0..self.rows as usize {
                        for j in cols..=ord_x2 as usize {
                            self.data.insert((ord_x2 as usize + 1) * i + j, 0)
                        }
                    }

                    cols = ord_x2 as usize + 1;
                }

                if ord_y2 + 1 > self.rows as i32 {
                    let target_cols = cmp::max(ord_x2 + 1, cols as i32);
                    for _i in self.rows as i32..=ord_y2 {
                        for _ in 0..target_cols {
                            self.data.push(0);
                        }
                    }

                    self.rows = ord_y2 as u32 + 1;
                }

                //sign the line

                match (x1,y1,x2,y2) {
                    (a,_,c,_) if a == c => {                        
                        for i in ord_y1..=ord_y2 {
                            self.data[cols * i as usize + ord_x1 as usize] += 1
                        } 
                    },
                    (_,b,_,d) if b == d => {
                        for i in ord_x1..=ord_x2 {
                            self.data[cols * ord_y1 as usize + i as usize] += 1
                        }
                    },
                    (a,b,c,d) if ((c - a) / (d - b)).abs() == 1 => {
                        let sign : i32 = (c - a) / (d - b);

                        let x_begin = cmp::min(a,c);
                        let y_begin = match sign { 1 => cmp::min(b,d), -1 => cmp::max(b,d), _ => b};

                        for i in 0..=(cmp::max(c,a) - cmp::min(a,c)) {
                            self.data[cols * (y_begin + (i * sign)) as usize + (x_begin + i) as usize] += 1;
                        }
                    }, 
                    _ => ()
                }

            }
            None => ()
        }
    }
}

// impl fmt::Display for Map {
//     fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
//         let cols = self.data.len() / self.rows as usize;

//         let mut formatted = String::new();

//         for i in 0..self.rows as usize {
//             formatted.push_str("[");

//             for j in 0..cols {
//                 formatted += self.data[cols * i + j].to_string().as_str();
//             }
            
//             formatted.push_str("]\n");
//         }

//         write!(f, "{}", formatted)
//     }
// }


fn main() {

    let content = fs::read_to_string("/Users/mae/Documents/Personal/Programming/rust/aoc_2021/aoc_5/src/input")
        .expect("Error while reading the file");

    let mut coordinates = content.split_terminator("\n");

    let mut map = Map::new();

    while let Some(c) = coordinates.next() {
        map.add_line(c);
    }

    let n = map.data.iter().filter(|c| **c > 1).count();

    println!("Rows: {} intersections: {}", map.rows, n);
}
