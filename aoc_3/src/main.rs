use std::fs;

fn main() {

    let content = fs::read_to_string("src/input")
    .expect("Something went wrong reading the file");

    //Part 1

    let line_numbers = content.as_str().split_terminator("\n").count();

    let gamma_rate_vec : Vec<u32> = content.as_str().split_terminator("\n")
        .map(|line| line.chars().map(|c| c.to_digit(10).unwrap()).collect::<Vec<u32>>())
        .fold(vec![], |mut acc, c| {
            println!("Processing {:?}", c);

            let mut iter_line = c.iter();
            let mut index = 0;

            while let Some(cc) = iter_line.next() {
                match acc.get_mut(index) {
                    Some(a) => *(a) += cc,
                    None => acc.push(*cc)
                };

                index += 1;
            }

            acc
        }).iter()
        .map(|v| if *v > line_numbers as u32 / 2 {1} else {0})
        .collect();

    let epsilon_rate_string = gamma_rate_vec.iter().map(|i| match i { 0 => 1, 1 => 0, _ => 0}).map(|i| i.to_string()).collect::<String>();
    let gamma_rate_string = gamma_rate_vec.iter().map(|i| i.to_string()).collect::<String>();

    let gamma_rate = isize::from_str_radix(gamma_rate_string.as_str(), 2).unwrap();
    let epsilon_rate = isize::from_str_radix(epsilon_rate_string.as_str(), 2).unwrap();

    println!("{} gamma: {}, epsilon {}", gamma_rate * epsilon_rate, gamma_rate, epsilon_rate);

    //Part 2

    let mut diagnostics : Vec<Vec<u32>> = content.as_str().split_terminator("\n")
        .map(|line| line.chars().map(|c| c.to_digit(2).unwrap()).collect::<Vec<u32>>()).collect();

    let oxygen_vec = extract(diagnostics.iter().map(|d| d).collect(), 0, 0);
    let co2_vec = extract(diagnostics.iter().map(|d| d).collect(), 0, 1);

    let oxygen_string = oxygen_vec[0].iter().map(|i| i.to_string()).collect::<String>();
    let co2_string = co2_vec[0].iter().map(|i| i.to_string()).collect::<String>();

    let oxygen_rate = isize::from_str_radix(oxygen_string.as_str(), 2).unwrap();
    let co2_rate = isize::from_str_radix(co2_string.as_str(), 2).unwrap();

    println!("{:?} {:?} {} {} {}", oxygen_vec, co2_vec, oxygen_rate, co2_rate, oxygen_rate * co2_rate);
} 

fn extract(mut diag : Vec<&Vec<u32>>, i : usize, sign : u32) -> Vec<&Vec<u32>> {
    if diag.len() == 1 || i == diag[0].len() {
        return diag;
    }

    //define target 

    let target = if diag.iter().fold(0, |acc, x| acc + x[i]) >= diag.len() as u32 / 2 {1 - sign} else {0 + sign};

    let mut target_diag = vec![];

    for d in diag.iter() {
        if d[i] == target {
            target_diag.push(*d);
        }
    }

    extract(target_diag, i + 1, sign)
}