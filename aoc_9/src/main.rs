use std::collections::HashSet;
use std::fs;


// struct SimpleTree {
//     root: Box<Node>
// }

// struct Node {
//     v: u32,
//     leaves: Vec<Box<Node>>
// }

// impl Node {
//     fn new(value: u32, ) -> Node {
//         Node {

//         }
//     }
// }

fn main() {

    let content = fs::read_to_string("src/input").expect("Err while reading the file");

    let mut height_map : Vec<Vec<i32>> = content.split_terminator("\n").map(|l| l.chars().map(|c| c.to_digit(10).unwrap() as i32).collect())
        .collect();

    //Padding with 9s
    let cols = height_map[0].len();
    let rows = height_map.len();

    
    for l in height_map.iter_mut() {
        l.insert(0, 9);
        l.push(9);
    }
    let _first_line = height_map.insert(0, vec![9; cols + 2]);
    let _last_line = height_map.push(vec![9; cols + 2]);

    // for l in height_map.iter() {
    //     println!("{:?}", l);
    // }

    let mut total = vec![];

    for i in 1..=rows {
        for j in 1..=cols {
            total.push(match (i,j,height_map[i][j]) {
                (_,_,t) => if t < height_map[i][j+1] && t < height_map[i+1][j] && t < height_map[i-1][j] && t < height_map[i][j-1] {(t,i,j)} else {(-1,i,j)}
            })
        }
    }

    let mut basin_cardinality = vec![];
    
    for low_point in total.iter().filter(|t| t.0 >= 0) {
        let mut basin : HashSet<(usize, usize)> = HashSet::new();

        explore_surroundings((low_point.1, low_point.2), &height_map, &mut basin);

        basin_cardinality.push(basin.len());

        //println!("{:?} -> {:?} -> {:?}", low_point, basin, basin.iter().map(|p| height_map[p.0][p.1]).collect::<Vec<i32>>());
    }

    println!("{:?} sum: {}", total.iter().filter(|t| t.0 >= 0).collect::<Vec<&(i32,usize,usize)>>(), total.iter().map(|t| t.0 + 1).sum::<i32>());


    basin_cardinality.sort();
    basin_cardinality.reverse();


    println!("{:?}", basin_cardinality[0..3].iter().product::<usize>());
}


fn explore_surroundings(p: (usize,usize), map: &Vec<Vec<i32>>, basin: &mut HashSet<(usize,usize)>) -> () {

    basin.insert(p);

    match (map[p.0 + 1][p.1],map[p.0 - 1][p.1],map[p.0][p.1 + 1],map[p.0][p.1 - 1]) {
        (9,9,9,9) => return,
        (a,b,c,d) => {
            if a != 9 && !basin.contains(&(p.0 + 1, p.1)) {
                explore_surroundings((p.0 + 1, p.1), map, basin);
            }
            if b != 9 && !basin.contains(&(p.0 - 1, p.1)) {
                explore_surroundings((p.0 - 1, p.1), map, basin);
            }
            if c != 9 && !basin.contains(&(p.0, p.1 + 1)) {
                explore_surroundings((p.0, p.1 + 1), map, basin);
            }
            if d != 9 && !basin.contains(&(p.0, p.1 - 1)){
                explore_surroundings((p.0, p.1 - 1), map, basin);
            }
        }
    }
}
